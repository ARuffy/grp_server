-module(sock_srv_tcp).
-behaviour(gen_server).

-export([start_link/0]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
          code_change/3, terminate/2]).

-define(ACCEPT_TIMEOUT, 5000).
-record(state, {socket, options}).

%%%============================================================================
start_link() ->
  gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

init([]) ->
  gen_server:cast(self(), start),
  {ok, #state{}}.

%%%============================================================================
handle_call(stop, _From, State) ->
  lager:debug("TCP_Server: Stop"),
  {stop, normal, ok, State};

handle_call(_E, _From, Socket) ->
  {noreply, Socket}.

%%%============================================================================
handle_cast(start, State) ->
  lager:debug("TCP_Server: Start"),
  case application:get_env(tcp) of
    {ok, TCP_Options} ->
      gen_server:cast(self(), listen),
      {noreply, State#state{options = TCP_Options}};

    {error, Reason} ->
      {stop, Reason, State}
  end;

handle_cast(listen, State = #state{options = TCP_Options}) ->
  TCP_Port = proplists:get_value(port, TCP_Options, 8881),
  case parse_options(TCP_Options) of
    {ok, TCP_SocketOptions} ->
      case gen_tcp:listen(TCP_Port, TCP_SocketOptions) of
        {ok, TCP_ListenSocket} ->
          lager:info("TCP_Server: LISTEN port:~p", [TCP_Port]),
          gen_server:cast(self(), accept),
          {noreply, State#state{socket = TCP_ListenSocket}};

        {error, Reason} ->
          {stop, {listen, Reason}, State}
      end;

    {error, Reason} ->
      {stop, {listen, Reason}, State}
  end;

handle_cast(accept, State = #state{socket = TCP_ListenSocket}) ->
  case gen_tcp:accept(TCP_ListenSocket, ?ACCEPT_TIMEOUT) of
    {ok, TCP_Socket} ->
      sock_sess_mngr:channel_connection(TCP_Socket),
      gen_server:cast(self(), accept),
      {noreply, State};

    {error, timeout} ->
      gen_server:cast(self(), accept), %% to catch stop signal
      {noreply, State};

    {error, Reason} ->
      lager:error("TCP_Server: Accept error : ~p", [Reason]),
      {stop, {accept, Reason}, State}
  end;

handle_cast(_Message, S) ->
  {noreply, S}.
%%%============================================================================
handle_info({disconnect, _Pid, normal}, S) ->
  {noreply, S};

handle_info({disconnect, Pid, Reason}, S) ->
  lager:warning("TCP_Server: child ~p disconnect ~p", [Pid, Reason]),
  {noreply, S};

handle_info(_Error, S) ->
  {noreply, S}.

%%%============================================================================
code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

terminate(normal, _State) -> ok;

terminate(Reason, _State) ->
  lager:warning("TCP_Server: Terminate ~p", [Reason]).

%%%============================================================================
parse_options( Options ) when is_list( Options ) ->
  {ok, [binary, {packet, raw}, {active, false}]}.

%%%============================================================================