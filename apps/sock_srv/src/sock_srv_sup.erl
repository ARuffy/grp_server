-module(sock_srv_sup).
-behaviour(supervisor).

-export([start_link/0, init/1]).

-define(SNAME, ?MODULE).
-define(MAX_RESTART, 10).
-define(MAX_RESTART_TIME, 5000).
-define(SHATDOWN_TIME, 10000).

%%%============================================================================
start_link() ->
    supervisor:start_link({local, ?SNAME}, ?MODULE, []).

init([]) ->
    {ok, {{one_for_one, ?MAX_RESTART, ?MAX_RESTART_TIME}, [
        {sock_srv_ssl,
            {sock_srv_ssl, start_link, []},
            permanent, ?SHATDOWN_TIME, worker, [sock_srv_ssl]},

        {sock_srv_tcp,
            {sock_srv_tcp, start_link, []},
            permanent, ?SHATDOWN_TIME, worker, [sock_srv_tcp]}
    ]}}.

%%%============================================================================
