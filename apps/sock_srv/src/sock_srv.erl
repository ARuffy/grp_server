-module(sock_srv).
-behaviour(application).

-export([start/2, stop/1]).

%%%============================================================================
start(normal, _Args) ->
    sock_srv_sup:start_link().

stop(_State) -> ok.

%%%============================================================================

