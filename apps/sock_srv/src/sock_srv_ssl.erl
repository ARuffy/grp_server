-module(sock_srv_ssl).
-behaviour(gen_server).

-export([start_link/0]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
          code_change/3, terminate/2]).

-define(ACCEPT_TIMEOUT, 5000).
-record(state, {socket, options}).

%%%============================================================================
start_link() ->
  gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

init([]) ->
  gen_server:cast(self(), start),
  {ok, #state{}}.

%%%============================================================================
handle_call(stop, _From, State) ->
  lager:debug("SSL_Server:Main.Stop"),
  {stop, normal, ok, State};

handle_call(_E, _From, Socket) ->
  {noreply, Socket}.

%%%============================================================================
handle_cast(start, State) ->
  lager:debug("SSL_Server: Start"),
  case application:get_env(ssl) of
    {ok, SSL_Options} ->
      gen_server:cast(self(), listen),
      {noreply, State#state{options = SSL_Options}};

    {error, Reason} ->
      {stop, Reason, State}
  end;

handle_cast(listen, State = #state{options = SSL_Options}) ->
  SSL_Port = proplists:get_value(port, SSL_Options, 8880),
  case parse_options(SSL_Options) of
    {ok, SSL_SocketOptions} ->
      lager:info("SSL_Server: LISTEN port:~p", [SSL_Port]),
      case ssl:listen(SSL_Port, SSL_SocketOptions) of
        {ok, SSL_ListenSocket} ->
          gen_server:cast(self(), accept),
          {noreply, State#state{ socket = SSL_ListenSocket }};

        {error, Reason} ->
          {stop, {listen, Reason}, State}
      end;

    {error, Reason} ->
      {stop, Reason, State}
  end;

handle_cast(accept, State = #state{socket = SSL_ListenSocket}) ->
  case ssl:transport_accept(SSL_ListenSocket, ?ACCEPT_TIMEOUT) of
    {ok, TransportSocket} ->
      case ssl:handshake(TransportSocket) of
        {ok, SSL_Socket} ->
          sock_sess_mngr:new_session(SSL_Socket);

        {error, Reason} ->
          lager:error("SSL_Server: Handshake failed: ~p", [Reason])
      end,
      gen_server:cast(self(), accept),
      {noreply, State};

    {error, timeout} ->
      gen_server:cast(self(), accept), %% to catch stop signal
      {noreply, State};

    {error, Reason} ->
      lager:error("SSL_Server Accept error : ~p", [Reason]),
      {stop, {accept, Reason}, State}
  end;

handle_cast(_Message, S) ->
  {noreply, S}.

%%%============================================================================
handle_info({disconnect, _Pid, normal}, S) ->
  {noreply, S};

handle_info({disconnect, Pid, Reason}, S) ->
  lager:warning("SSL_Server: child ~p disconnect ~p", [Pid, Reason]),
  {noreply, S};

handle_info(_Error, S) ->
  {noreply, S}.

%%%============================================================================
code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

terminate(normal, _State) -> ok;
terminate(Reason, _State) ->
  lager:warning("SSL_Server: Terminate ~p", [Reason]).

%%%============================================================================
parse_options(Options) when is_list(Options) ->
  KeyPath = proplists:get_value(keyfile, Options),
  CertPath = proplists:get_value(certfile, Options),
  {ok, [{keyfile, KeyPath},
        {certfile, CertPath},
        {versions, ['tlsv1.3']},
        {active, false}] };

parse_options(Options) ->
  lager:error("SSL_Server: Invalid options ~n~p", [Options]),
  {error, invalid_format}.

%%%============================================================================