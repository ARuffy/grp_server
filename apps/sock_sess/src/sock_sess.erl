-module(sock_sess).
-behaviour(application).

-export([start/2, stop/1]).

%%%============================================================================
start(normal, _Args) ->
  sock_sess_sup:start_link().

stop(_State) -> ok.

%%%============================================================================