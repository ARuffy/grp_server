-module(sock_sess_cns_sup).
-behaviour(supervisor).

-export([start_link/0, init/1]).

-define(MAX_RESTART, 5).
-define(MAX_RESTART_TIME, 5000).
-define(SHATDOWN_TIME, 3000).

%%============================================================================
start_link() ->
  supervisor:start_link({local, ?MODULE}, ?MODULE, []).

init([]) ->
  {ok, {{simple_one_for_one, ?MAX_RESTART, ?MAX_RESTART_TIME}, [
    {sock_sess_connect_sup,
          {sock_sess_connect_sup, start_link, []},
          temporary, 3000, supervisor, [sock_sess_connect_sup]}
    ]}}.

%%%============================================================================
