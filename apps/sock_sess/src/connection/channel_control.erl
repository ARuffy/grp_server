-module(channel_control).
-behavior(gen_server).

-include_lib("public_key/include/public_key.hrl").
-include_lib("protocol/Protocol.hrl").

-export([start_link/1]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
          code_change/3, terminate/2]).

-record(state, {session_id, socket, controller}).

%% ----------------------------------------------------------------------------
start_link(Args) ->
  gen_server:start_link(?MODULE, Args, []).

init({SessionID, ConnectionController}) ->
  {ok, #state{session_id = SessionID, controller = ConnectionController}}.

%% ----------------------------------------------------------------------------
handle_call(_Request, _From, State) ->
  {reply, ok, State}.

%% ----------------------------------------------------------------------------
handle_cast({start, SSL_Socket, RSA_Key, Key128}, State) ->
  gen_server:cast(self(), {receive_session_command, RSA_Key, Key128}),
  %% @todo: save key in state
  %% @todo: wtf with Key128?
  {noreply, State#state{socket = SSL_Socket}};

handle_cast({receive_session_command, RSA_Key, Key128}, State = #state{socket = SSL_Socket}) ->
  case ssl:recv(SSL_Socket, 0) of
    {ok, Data} ->
        %todo: handler of protocol messages
        case 'Protocol':decode('SessionOpenReq', Data) of
            {ok, Message} ->
                lager:info( "SessionOpenReq: client <~p>", [Message#'SessionOpenReq'.client] ),
                {ok, OpenSessionRes} = 'Protocol':encode('SessionOpenRes',
                  #'SessionOpenRes'{result = true, session = State#state.session_id}),
                ssl:send(SSL_Socket, OpenSessionRes),
                gen_server:cast(self(), {receive_channel_command, RSA_Key, Key128}),
                {noreply, State};

            {error, _Reason} ->
                {stop, {invalid_message, asn1}, State}
        end;

    {error, closed} ->
      {stop, normal, State};

    {error, Reason} ->
      {stop, Reason, State}
  end;

handle_cast({receive_channel_command, RSA_Key, Key128}, State = #state{socket = SSL_Socket}) ->
  case ssl:recv(SSL_Socket, 0) of
    {ok, Data} ->
        %todo: handler of protocol messages
        case 'Protocol':decode('SessionOpenReq', Data) of
            {ok, _Message} ->
                lager:info( "ChannelOpenReq: recieved" ),
                {ok, ChannelOpenRes} = 'Protocol':encode('ChannelOpenRes',
                  #'ChannelOpenRes'{result = true,
                                    modulus = RSA_Key#'RSAPublicKey'.modulus,
                                    exponent = RSA_Key#'RSAPublicKey'.publicExponent,
                                    algorithm = #'ProtoAlgorithm'{type = aes, mode = ecb},
                                    key = Key128}),
                ssl:send(SSL_Socket, ChannelOpenRes),
                gen_server:cast(self(), {receive_channel_command, RSA_Key, Key128}),
                {noreply, State};

            {error, _Reason} ->
                {stop, {invalid_message, asn1}, State}
        end;

    {error, closed} ->
      {stop, normal, State};

    {error, Reason} ->
      {stop, Reason, State}
  end;

handle_cast(Message, S) ->
  lager:warning("Skip cast ~p", [Message]),
  {noreply, S}.

%% ----------------------------------------------------------------------------
handle_info(Error, S) ->
  lager:warning("Skip unexpected info: ~p", [Error]),
  {noreply, S}.

%% ----------------------------------------------------------------------------
code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

terminate(Reason, State) ->
  lager:debug("Terminate: ~p", [Reason]),
  close_connection(State, Reason).

%% ----------------------------------------------------------------------------
close_connection(State, Reason) ->
  ssl:close(State#state.socket),
  gen_server:cast(State#state.controller, {disconnect, control, Reason}).
