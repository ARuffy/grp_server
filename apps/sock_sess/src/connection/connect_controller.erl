-module(connect_controller).
-behaviour(gen_server).

-export([start_link/2]).
-export([init/1,
    handle_call/3, handle_cast/2, handle_info/2,
    code_change/3, terminate/2]).

-record(state, {sess_id, supervisor, key, channel_ctrl, channel_data}).
%%%============================================================================
start_link(Supervisor, SessionID) ->
  gen_server:start_link(?MODULE, {Supervisor, SessionID}, []).

init({Supervisor, SessionID}) ->
  gen_server:cast(self(), start),
  {ok, #state{sess_id = SessionID,
              supervisor = Supervisor,
              key = crypto:strong_rand_bytes(16)}}.

%%%============================================================================
handle_call(Message, From, State) ->
  lager:warning("Skip call from ~p : ~p", [From, Message]),
  {noreply, State}.

%%%============================================================================
handle_cast(start, State = #state{sess_id = SessionID}) ->
  lager:debug("Start <~.16b>", [SessionID]),
  sock_sess_mngr:connection_created({SessionID, self()}),
  {ok, ControlChannel} = supervisor:start_child(State#state.supervisor,
    {channel_control,
        {channel_control, start_link, [{SessionID, self()}]},
        temporary, 3000, worker, [channel_control]} ),
  {noreply, State#state{channel_ctrl = ControlChannel}};

handle_cast({connection, SSL_Socket, RSA_Key}, State = #state{channel_ctrl = Channel}) ->
  lager:info("Session <~.16b> connection established", [State#state.sess_id]),
  gen_server:cast(Channel, {start, SSL_Socket, RSA_Key, State#state.key}),
  {ok, DataChannel} = supervisor:start_child(State#state.supervisor,
    {channel_data,
        {channel_data, start_link, [{State#state.sess_id, State#state.key}]},
        temporary, 3000, worker, [channel_data]} ),
  {noreply, State#state{channel_data = DataChannel, key = State#state.key}};

handle_cast({channel, TCP_Socket}, State = #state{channel_data = Channel}) ->
  lager:info("Session <~.16b> new channel opened", [State#state.sess_id]),
  gen_server:cast(Channel, {start, TCP_Socket}),
  {noreply, State};

handle_cast({disconnect, control, Reason}, State) ->
  sock_sess_mngr:close_session(State#state.sess_id),
  {stop, Reason, State};

handle_cast(Message, S) ->
  lager:warning("Skip cast ~p", [Message]),
  {noreply, S}.

%%%============================================================================
handle_info(Error, S) ->
  lager:warning("Skip unexpected info: ~p", [Error]),
  {noreply, S}.

%%%============================================================================
code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

terminate(normal, _State) -> ok;

terminate(Reason, _State) ->
  lager:warning("Terminate: ~p", [Reason]).

%%%============================================================================