-module(channel_data).
-behavior(gen_server).

-include_lib("protocol/Protocol.hrl").

-export([start_link/1]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
          code_change/3, terminate/2]).

-record(state, {session_id, socket, key}).

%% ----------------------------------------------------------------------------
start_link(Args) ->
  gen_server:start_link(?MODULE, Args, []).

init({SessionID, Key128}) ->
  {ok, #state{session_id = SessionID, key = Key128}}.

%% ----------------------------------------------------------------------------
handle_call(_Request, _From, State) ->
  {reply, ok, State}.

%% ----------------------------------------------------------------------------
handle_cast({start, TCP_Socket}, State = #state{key = Key128}) ->
  % @todo: check old session
  {ok, ChannelConnectionRes} = 'Protocol':encode('ChannelConnectionRes',
    #'ChannelConnectionRes'{result = true}),

  AES_BlockSize = 128,
  ZeroPadding = AES_BlockSize - (bit_size(ChannelConnectionRes) rem AES_BlockSize),
  Data = <<ChannelConnectionRes/binary, 0:ZeroPadding>>,
  Message = crypto:crypto_one_time(aes_128_ecb, Key128, Data, true),
  gen_tcp:send(TCP_Socket, Message),
  gen_server:cast(self(), receive_command),
  {noreply, State#state{socket = TCP_Socket}};

handle_cast(receive_command, State = #state{socket = TCP_Socket}) ->
  case gen_tcp:recv(TCP_Socket, 0) of
    {ok, Data} ->
        Message = crypto:crypto_one_time(aes_128_ecb, State#state.key, Data, false),
        lager:debug( "Channel_Data: Message ~p", [Message] ),
        gen_tcp:send(TCP_Socket, Message),
        gen_server:cast(self(), receive_command),
        {noreply, State};

    {error, closed} ->
      {stop, normal, State};

    {error, Reason} ->
      {stop, Reason, State}
  end;

handle_cast(Message, S) ->
  lager:warning("Channel_Data: skip cast from ~p", [Message]),
  {noreply, S}.

%% ----------------------------------------------------------------------------
handle_info(Error, S) ->
  lager:warning("Channel_Data: skip unexpected info: ~p", [Error]),
  {noreply, S}.

%% ----------------------------------------------------------------------------
code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

terminate(Reason, _State = #state{socket = TCP_Socket}) ->
  case TCP_Socket of
    undefined-> ok;
    _ -> gen_tcp:close(TCP_Socket)
  end,
  % todo: inform channel controller
  lager:debug("Terminate: ~p", [Reason]).

%% ----------------------------------------------------------------------------
