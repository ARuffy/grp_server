-module(sock_sess_sup).
-behaviour(supervisor).

-export([start_link/0, init/1]).

-define(MAX_RESTART, 10).
-define(MAX_RESTART_TIME, 5000).
-define(SHATDOWN_TIME, 3000).

%%%============================================================================
start_link() ->
  supervisor:start_link({local, ?MODULE}, ?MODULE, []).

init(_Args) ->
  {ok, {{one_for_all, ?MAX_RESTART, ?MAX_RESTART_TIME},
    [ {sock_sess_mngr,
        {sock_sess_mngr, start_link, [self()]},
        permanent, ?SHATDOWN_TIME, worker, [sock_sess_mngr]}
    ]}}.

%%%============================================================================
