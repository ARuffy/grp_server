-module(sock_sess_mngr).
-behaviour(gen_server).

-include_lib("public_key/include/public_key.hrl").

-define(TCP_CONNECTION_POOL, channel_connection_pool).

-export([new_session/1, close_session/1,
  connection_created/1, channel_connection/1]).

-export([start_link/1, init/1,
    handle_call/3, handle_cast/2, handle_info/2,
    code_change/3, terminate/2]).

-record(state, {supervisor, rsa_key, session_table, session}).
-record(session, {id, key, controller = undefined}).

%%% API =======================================================================
new_session(SSL_Socket) ->
  gen_server:cast(?MODULE, {new_session, SSL_Socket}).

close_session(SessionID) ->
  gen_server:cast(?MODULE, {close_session, SessionID}).

connection_created(SessionInformation) ->
  gen_server:cast(?MODULE, {connection_created, SessionInformation}).

channel_connection(TCP_Socket) ->
  gen_server:cast(?MODULE, {channel_connection, TCP_Socket}).
%%%============================================================================
start_link(Supervisor) ->
  gen_server:start_link({local, ?MODULE}, ?MODULE, Supervisor, []).

init(Supervisor) ->
  PoolSize = application:get_env(sock_sess, pool_size, 5),
  ppool:start_pool(?TCP_CONNECTION_POOL, PoolSize, {channel_connector, start, []}),
  gen_server:cast(self(), start),
  {ok, #state{
    supervisor = Supervisor,
    session_table = ets:new(socket_session_table, [{keypos, 2}])
  }}.

%%%============================================================================
handle_call(Message, From, State) ->
  lager:warning("Skip call from ~p : ~p", [From, Message]),
  {noreply, State}.

%%%============================================================================
handle_cast(start, State) ->
  {ok, RSA_Key} = create_key(),
  {ok, Supervisor} = supervisor:start_child(State#state.supervisor,
    {sock_sess_cns_sup,
      {sock_sess_cns_sup, start_link, []},
      temporary, 3000, supervisor, [sock_sess_cns_sup]}),
  gen_server:cast(self(), create_session),
  {noreply, State#state{supervisor = Supervisor, rsa_key = RSA_Key}};

handle_cast({new_session, SSL_Socket}, State = #state{session = Session}) ->
  case Session#session.controller of
    undefined ->
      lager:warning("Session <~.16b> controller is null", [Session#session.id]),
      {noreply, State};

    Pid ->
      RSA_PrivateKey = State#state.rsa_key,
      RSA_PublicKey = #'RSAPublicKey'{modulus = RSA_PrivateKey#'RSAPrivateKey'.modulus,
        publicExponent = RSA_PrivateKey#'RSAPrivateKey'.publicExponent},
      gen_server:cast(Pid, {connection, SSL_Socket, RSA_PublicKey}),
      gen_server:cast(self(), create_session),
      {noreply, State#state{session = #session{}}}
  end;

handle_cast(create_session, State = #state{session_table = SessionTable}) ->
  case create_session(SessionTable) of
    {ok, Session} ->
      lager:info("Create new session <~.16b>", [Session#session.id]),
      {ok, _Pid} = supervisor:start_child(State#state.supervisor, [Session#session.id]),
      {noreply, State#state{session = Session}};

    {error, Reason} ->
      lager:error("Create new session failed: ~p", [Reason]),
      {noreply, State}
  end;

handle_cast({close_session, SessionID}, State = #state{session_table = SessionTable}) ->
  ets:delete(SessionTable, SessionID),
  lager:info("Close session: <~.16b>", [SessionID]),
  {noreply, State};

handle_cast({channel_connection, TCP_Socket}, State = #state{rsa_key = RSA_Key}) ->
  ppool:async_queue(?TCP_CONNECTION_POOL, [self(), TCP_Socket, RSA_Key]),
  {noreply, State};

handle_cast({open_channel, TCP_Socket, SessionID}, State) ->
  case find_session(State#state.session_table, SessionID) of
    {ok, Session} ->
      gen_server:cast(Session#session.controller, {channel, TCP_Socket});

    {error, _Reason} ->
      gen_tcp:close(TCP_Socket), %todo: send responce?
      lager:error("Open channel failed: session <~.16b> isn't exist", [SessionID])
  end,
  {noreply, State};

handle_cast({connection_created, {SessionID, Pid}}, State) ->
  case find_session(State#state.session_table, SessionID) of
    {ok, S} ->
      Session = S#session{controller = Pid},
      ets:insert(State#state.session_table, Session),
      {noreply, State#state{session = Session}};

    {error, Reason} ->
      lager:error("Create connection <~.16b> failed: ~p",
        [SessionID, Reason]),
      {noreply, State}
  end;

handle_cast(Message, S) ->
  lager:warning("Skip cast from ~p", [Message]),
  {noreply, S}.

%%%============================================================================
handle_info(Error, S) ->
  lager:warning("Session_Manager skip unexpected info: ~p", [Error]),
  {noreply, S}.

%%%============================================================================
code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

terminate(normal, _State) -> ok;

terminate(Reason, _State) ->
  lager:warning("Session_Manager terminate: ~p", [Reason]).

%%%============================================================================
create_session(SessionTable) ->
  <<A:32, B:32, C:32, D:32>> = crypto:strong_rand_bytes(16),
  rand:seed(exsp, {A, B, C}),
  F = ( rand:uniform(16#ffffffff) bxor D ),
  <<SessionID:64>> = <<F:16, A:16, B:8, C:8, F:8, D:8>>,

  case ets:lookup(SessionTable, SessionID) of
    [] ->
      Key128 = <<D:32, A:32, F:32, B:32>>, %% @todo: generate key
      Session = #session{id = SessionID, key = Key128},
      ets:insert(SessionTable, Session),
      {ok, Session};

    [Session|_] ->
      {error, {exist, Session}}
  end.

create_key() ->
  RSA_Key = public_key:generate_key({rsa, 1024, 85627}),
  lager:debug("Generate Key: ~p", [RSA_Key]),
  {ok, RSA_Key}.

%%%============================================================================

find_session(Table, SessionID) ->
   case ets:lookup(Table, SessionID) of
    [] -> {error, not_found};
    [Session|_] -> {ok, Session}
  end.

%%%============================================================================