PROTO_COMPILE_DIR := ./apps/sock_sess/src/connection/protocol/asn1
PROTO_OUTDIR := ./client

compile_proto:
	erlc -o ${PROTO_OUTDIR} ${PROTO_COMPILE_DIR}/Protocol.asn1
	@cp -v ${PROTO_OUTDIR}/Protocol.erl ${PROTO_OUTDIR}/Protocol.hrl ./apps/sock_sess/src/connection/protocol/

compile: compile_proto
	@./rebar3 as develop release
