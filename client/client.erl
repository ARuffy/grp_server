-module(client).
-export([start/3, start_pool/1]).

-include_lib("public_key/include/public_key.hrl").
-include_lib("Protocol.hrl").

-define(RECV_TIMEOUT, 3000).

start(SSL_Port, TCP_Port, ClientID) ->
    ssl:start(),
    crypto:start(),

    % SSL connection and open session
    {ok, SSL_Socket} = ssl:connect(localhost, SSL_Port, [{versions, ['tlsv1.3']},{active, false}]),
    {ok, SessionOpenReq} = 'Protocol':encode('SessionOpenReq', #'SessionOpenReq'{
        client = ClientID, version = 1 }),
    ssl:send(SSL_Socket, SessionOpenReq),
    {ok, Response} = ssl:recv(SSL_Socket, 0),
    {ok, OpenSessionRes} = 'Protocol':decode('SessionOpenRes', Response),
    SessionID = OpenSessionRes#'SessionOpenRes'.session,
    io:fwrite("Client#~p\tSSL Connected: session <~.16b>.~n", [ClientID, SessionID]),

    % Request of open channel
    {ok, ChannelOpenReq} = 'Protocol':encode('ChannelOpenReq', #'ChannelOpenReq'{ session = SessionID }),
    ssl:send(SSL_Socket, ChannelOpenReq),
    {ok, ChannelOpenResponse} = ssl:recv(SSL_Socket, 0, ?RECV_TIMEOUT),
    {ok, ChannelOpenRes} = 'Protocol':decode('ChannelOpenRes', ChannelOpenResponse),

    Result = ChannelOpenRes#'ChannelOpenRes'.result,
    RSA_Key = #'RSAPublicKey'{ modulus = ChannelOpenRes#'ChannelOpenRes'.modulus,
        publicExponent = ChannelOpenRes#'ChannelOpenRes'.exponent },

    io:fwrite("Client#~p\tChannelOpenRes result: ~p.~n", [ClientID, Result]),
    io:fwrite("Client#~p\tChannelOpenRes Public Key: ~p.~n", [ClientID, RSA_Key]),

    SymmetricAlgorithm = { ChannelOpenRes#'ChannelOpenRes'.algorithm#'ProtoAlgorithm'.type,
        ChannelOpenRes#'ChannelOpenRes'.algorithm#'ProtoAlgorithm'.mode },
    SymmetricKey = ChannelOpenRes#'ChannelOpenRes'.key,

    io:fwrite("Client#~p\tAlgorithm: ~p \tKey: ~p.~n", [ClientID, SymmetricAlgorithm, SymmetricKey]),

    %%% AES_BlockSize = 128,
    %%% ZeroPadding = AES_BlockSize - (bit_size(ChannelConnectionReq) rem AES_BlockSize),
    %%% BlockChannelConnectionReq = <<ChannelConnectionReq/binary, 0:ZeroPadding>>,
    %%% OpenReqData = crypto:crypto_one_time(aes_128_ecb, Key, BlockChannelConnectionReq, true),

    % Start TCP connection: send ChannelOpenReq
    {ok, TCP_Socket} = gen_tcp:connect(localhost, TCP_Port, [binary, {active, false}]),
    {ok, ChannelConnectionReq} = 'Protocol':encode('ChannelConnectionReq',
        #'ChannelConnectionReq'{ session = SessionID }),
    CCReq_Data = public_key:encrypt_public(ChannelConnectionReq, RSA_Key),
    gen_tcp:send(TCP_Socket, CCReq_Data),

    % Get answer: recv ChannelResponse
    {ok, CCRes_Data} = gen_tcp:recv(TCP_Socket, 0, ?RECV_TIMEOUT),
    ChannelConnectionRes = crypto:crypto_one_time(aes_128_ecb, SymmetricKey, CCRes_Data, false),
    {ok, CCRes} = 'Protocol':decode('ChannelConnectionRes', ChannelConnectionRes),
    io:fwrite("Client#~p\tChannel open result: ~p.~n",
        [ClientID, CCRes#'ChannelConnectionRes'.result]),

    send_message({TCP_Socket, ClientID}, SymmetricKey, <<"Hello, server..">>, 3),
    gen_tcp:close(TCP_Socket),
    ssl:close(SSL_Socket).

%%=================================================================================================
send_message(_Socket, _Key, _Message, 0) -> ok;

send_message({TCP_Socket, ClientID}, Key, Message, Count) when Count > 0 ->
    Number = integer_to_binary(Count),
    Msg = <<Message/binary, Number/binary>>,
    echo_message({TCP_Socket, ClientID}, Key, Msg),
    send_message({TCP_Socket, ClientID}, Key, Message, Count - 1).

%%=================================================================================================
echo_message({TCP_Socket, ClientID}, Key, Message) ->
    % Send message
    io:fwrite("Client#~p\tSend Message: ~p.~n", [ClientID, Message]),
    MessageData = crypto:crypto_one_time(aes_128_ecb, Key, Message, true),
    gen_tcp:send(TCP_Socket, MessageData),

    % Recv message back
    {ok, Data} = gen_tcp:recv(TCP_Socket, 0, ?RECV_TIMEOUT),
    io:fwrite("Client#~p\tRecv Message: ~p.~n", [ClientID, Data]).
    %% Message = Data. % check data ident
%%=================================================================================================
start_pool(0) -> ok;
start_pool(Count) when Count > 0 ->
    erlang:spawn(?MODULE, start, [8800, 8810, Count]),
    start_pool( Count - 1 ).